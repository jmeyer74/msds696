#!/usr/bin/env python3

# run with
# ./temp-yaml-gen.py < temps_to_add.txt | grep -v metrics >> exporter_config.yml

import sys, re, yaml
from collections import OrderedDict

# - key: 'eaton.ups.output.current.[*]'
#     name: 'ups_output_amps'
#     labels:
#     app: $1

config = []
dedupe = {}
for line in sys.stdin:
    m = re.search(r'^([\w\d\-]+)\s+([\w\d\-\[\]\.]*) = .*?$', line)
    # print(line)
    obj = {} #OrderedDict()
    obj['key'] = m.group(2)
    obj['name'] = m.group(1)


    #m = re.search(r'.*\[(.*)\]', obj['key'])
    # if m:
    #     obj['key'] = re.sub(r'\[(.*)\]', '[*]', obj['key'])
    #     #print("d" , obj['key'], m.group(1))
    #     obj['labels'] = {'app':'$1'}
    #     obj['name'] = obj['key']
    # else:
    #     print('s',  obj['key'])
    # print(m.group(1))
    # print(m.group(2))
    config.append(obj)
    #dedupe[obj['key']] = obj

#for k, v in dedupe.items():
    #v['key'] = k
#    config.append(v)
metrics = {'metrics':config}

class IndentDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(IndentDumper, self).increase_indent(flow, False)

def quoted_presenter(dumper, data):
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style="'")

class quoted(str):
    pass
yaml.add_representer(str, quoted_presenter)

print(yaml.dump(metrics, sort_keys=False, explicit_start=True))