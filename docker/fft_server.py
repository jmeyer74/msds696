#!/usr/bin/env python3

import time
import numpy as np
import pandas as pd
from datetime import datetime, timezone
from prometheus_client import start_http_server, Gauge, Enum
from prometheus_pandas import query
from scipy.fft import rfft, rfftfreq
import requests

class FFTMetrics:
    hstates=["healthy", "unhealthy"]

    def __init__(self, app_port=80, polling_interval_seconds=5):
        #self.app_port = app_port
        self.polling_interval_seconds = polling_interval_seconds
        self.start_time = time.time()

        # Prometheus metrics to collect
        self.service_start   = Gauge("startup_utc", "Service startup time in UTC")
        self.service_last    = Gauge("last_scrpte_utc", "Last scrape time UTC")
        self.scrape_interval = Gauge("prometheus_scrape_interval_seconds", "Prometheus Scrape Time in Seconds")
        self.fft             = Gauge("fft", "FFT signal processing calculation", ["sensor"])
        self.health          = Enum("scrape_health", "Health", states=self.hstates)

    def run_metrics_loop(self):
        while True:
            self.fetch()
            time.sleep(self.polling_interval_seconds)

    def fft_rtcalc(self):
        #this is reused code from the jupyter notebook
        prometheus_url = 'http://jmeyer-misc:9090'
        p = query.Prometheus(prometheus_url)
        # going to use 2h to roughly base this off the success found with 100 data samples
        # fft is not timeseries data aware
        df = p.query('temperature[120m]') #roughly 100 values (needed)

        df.rename(columns={
            df.columns[0]: '1',
            df.columns[1]: '2',
            df.columns[2]: '3',
            df.columns[3]: '4',
            df.columns[4]: '5',
            df.columns[5]: '6',
            df.columns[6]: '7',
        }, inplace=True)

        #data cleaning
        df = df[df > -100]

        window_sample_size = df.shape[0] #should have about 100 samples to examine

        ret = [0.0] * 7
        for i in range(1,8):
            col = str(i)
            yf = rfft(df.iloc[-window_sample_size:][[col]].values.flatten())

            ret[i-1] = abs(yf[2].real) # the 2nd sample is key here, packed in an array hence the [0] or flatten()

        return ret


    def fetch(self):
        # Update Prometheus metrics with application metrics
        self.service_start.set(self.start_time)
        self.service_last.set(time.time())
        val = self.fft_rtcalc()
        print(val)
        self.fft.labels(0).set(val[0])
        self.fft.labels(1).set(val[1])
        self.fft.labels(2).set(val[2])
        self.fft.labels(3).set(val[3])
        self.fft.labels(4).set(val[4])
        self.fft.labels(5).set(val[5])
        self.fft.labels(6).set(val[6])
        self.scrape_interval.set(self.polling_interval_seconds)
        self.health.state(self.hstates[0])

def main():
    # this could be made configurable via ENV
    # throttle the load put on prometheus here, :30 is about as fast as conceivably needed.
    polling_interval_seconds = 30
    exporter_port = 9191

    app_metrics = FFTMetrics(
        polling_interval_seconds = polling_interval_seconds
    )
    start_http_server(exporter_port)
    app_metrics.run_metrics_loop()

if __name__ == "__main__":
    main()