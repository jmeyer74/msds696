# MSDS 696 Powering Progress: Optimizing Energy Efficiency for Natural Gas-Powered GPUs
### Data Engineering Path
Justin Meyer

[jmeyer@crusoeenergy.com](mailto:jmeyer@crusoeenergy.com)

[jmeyer006@regis.edu](mailto:jmeyer006@regis.edu)

[Presentation YouTube](https://youtu.be/8f-Fo74chWs)

This is a follow up on [last semester's project](https://gitlab.com/jmeyer74/msds692)

## Crusoe Background

Crusoe Energy is a crypto startup that specializes in using the waste gas generated oil well
production to power crypto mining operations. Methane is 80x more effective as a greenhouse
gas than CO2, and thus the practice of flaring seemingly unusable gas is mandated by the EPA.
Instead of being flared, Crusoe uses this methane to power generators. This is a process they
call digital flare mitigation (DFM), which more completely combusts the methane compared to
a flare stack. The goal of the company is to reduce the need for traditional energy sources and
make crypto mining more sustainable and environmentally friendly.

Crusoe is in the process of expanding operations into a GPU cloud compute offering. Instead of
only powering ASIC crypto miners, Crusoe has begun installing mobile data units (MDU) to
remote locations in North Dakota. The GPU servers inside these mobile units may have a fixed
power capacity at full load, but it's not entirely clear how their heating needs will change in
different climates. These mobile units are equipped with air chillers, which must be configured
to maintain the ideal temperature range within the unit. However, the harsh weather
conditions in these remote locations, such as extremely hot summers and cold winters, can
greatly affect the power draw and cooling system performance. Therefore, gaining insight from
data center telemetry is essential for ensuring the success and efficiency of these mobile data
centers.

## Data Flow
### Data collection

Data collection outlined in a previous project: [https://gitlab.com/jmeyer74/msds692](https://gitlab.com/jmeyer74/msds692)

### Data Export, Cleaning and Re-import

The process of exporting the data was trivial using the [prometheus api](https://prometheus.io/docs/prometheus/latest/querying/api/).  
JSON is the default data type returned by prometheus, other options might be available. 

![export](screenshots/export.png)

Pictured above is the JSON object returned by prometheus with a simple query of `temperature[120m]`.  
This data is [easily ingested](https://gitlab.com/jmeyer74/msds696/-/blob/main/docker/fft_server.py#L34) into python using open source [prometheus pandas](https://gitlab.com/jmeyer74/msds696/-/blob/main/docker/fft_server.py#L8) library.

I chose not to scrub the bad temperature data (pictured below) from my ingestion.  From time to time, a single temperature unit reports a large negative value.  I find it helpful to leave the large sentinel negative values in the data set so that I can look into 
the frequency of these failures in the future

![baddata](screenshots/negTemps.png)

I believe this to be a single bad unit, but I'm keeping track of its failures for future analysis.  

These bad temperature values are scrubbed easily in pandas [here](https://gitlab.com/jmeyer74/msds696/-/blob/main/docker/fft_server.py#L51).  No more data scrubbing was needed.  This code is NaN tolerant. 

The result of the FFT analysis is re-ingested into prometheus with a scrape-able endpoint pictured below.  You can see the 2nd temp sensor is reporting a NaN due to a lack of sample during the screenshot, this is harmless in the data flow.

![scrape](screenshots/scrape.png)

The calculated values are displayed in the highlighted text above.   

The caclulated value axis on on the right of the graph below.  The "signal" shows up when the slow thrashing temperature is occuring.

![result](screenshots/result.png)

### Installation Notes

This phase of the project relies on a running installation of the previous project: [https://gitlab.com/jmeyer74/msds692](https://gitlab.com/jmeyer74/msds692)

It should also be noted that URLs are hard coded into this docker container for this project.  The URL "http://jmeyer-misc" is a host inside a corporate VPC.  

```
docker build .
docker run -d p9191:9191 <sha output from above command>
```

Not documented in the previous project, the prometheus [scrape config](https://gitlab.com/jmeyer74/msds692/-/blob/main/scripts/prometheus/prometheus.yml#L20) needs to include the output of the FFT service running on port 9191.
```
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: "zabbix_exporter"
    scrape_interval: 60s
    scrape_timeout: 30s
    metrics_path: /
    static_configs:
      - targets: ["jmeyer-misc:9224"]
  - job_name: "fft"
    scrape_interval: 10s
    metrics_path: /
    static_configs:
      - targets: ["jmeyer-misc:9191"]
```

## Weekly Updates

### Week 1
[Project Proposal](weekly_reports/week1proposal1.pdf)

### Week 2
[Update](weekly_reports/week2.pdf)

I have decided to [fork](https://github.com/turbohoje/zabbix-exporter) the Zabbix exporter to allow me to 
handle changing text values into Prometheus compatible floats.  
There is a key state stored in `eaton.ups.status["0"]` that holds what ATS state the UPS is in.  I was not able to find 
a similar or sister-value that stored this state numerically.  I am not able to reconfigure Zabbix to suit my needs.

Using the Zabbix exporter's command line options, I was able to dump all known vars into a [text file](dump_metrics.txt).

A simple `cat dump_metrics.txt| grep -i temp` of this file shows _many_ new interesting data points to scrape, store and trend.

I wish to improve my configuration to include all device temperatures that I can find.  These are to include switch temperatures as we just had a failure.

Also, I wish to compile and switch over to my fork of the Zabbix exporter once I get my build working.  This will allow me to collect the UPS/ATS state in Prometheus.

### Week 3
[Update](weekly_reports/week3.pdf)

There are additional temperature points I would like to track eventhough I may not do anything with them at the moment.  


### Week 4
[Update](weekly_reports/week4.pdf)

Additional data points are now being scraped from other SNMP devices.  I used [this script](./temp-yaml-gen.py) to generate a [more complete](./exporter_config.yml)
scrape config.

The exported data from Prometheus turned out to be in a floating-point format.  I did not trust my own code for converting this into a timezone-aware timestamp.  I spent some time visualizing, debugging and determining that my identified time segments of “sawtooth” temperatures were not lining up properly.  I did not begin doing any ML on my data after finding issues with my data labeling.

### Week 5
[Update](weekly_reports/week5.pdf)

Experimentation with Change-point detection.  

### Week 6
[Update](weekly_reports/week6.pdf)

Lots of exciting progress with Fourier transforms.

### Week7
[Update](weekly_reports/week7.pdf)

Make a queryable service: working on CSV export as an interactive test.  Fixed a nparray bug and confirmed working 2023-04-24.  Good data to visualize from this point forward.