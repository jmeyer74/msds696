#!/usr/bin/env python3

import csv
import requests
import sys
from datetime import datetime, timezone
from prometheus_api_client import PrometheusConnect

prometheus_url = 'http://jmeyer-misc:9090'
timerange      = '30d'

timerange      = '1h'


# pip install prometheus-connect
# https://github.com/4n4nd/prometheus-api-client-python

prom = PrometheusConnect(url = prometheus_url, disable_ssl=True)

def GetMetrixNames(url):
    response = requests.get('{0}/api/v1/label/__name__/values'.format(url))
    names = response.json()['data']
    return names

writer = csv.writer(sys.stdout)

metrixNames=GetMetrixNames(prometheus_url)
metrixNames = ['temperature']
#print(metrixNames)


writeHeader=True

for metrixName in metrixNames:
    m_list = prom.custom_query(query=metrixName+'['+timerange+']')

    for metric in m_list:
        #for safety, we are only coded for 'temperature'
        if metric['metric']['__name__'] == "temperature":
            print(metric['metric'])


    #for i in m[0]['values']:
    #    print(str( datetime.fromtimestamp(int(i[0]), timezone.utc)) + " " + str(i[1]))

#for metrixName in metrixNames:
    # response = requests.get('{0}/api/v1/query'.format(prometheus_url), params={'query': metrixName+'['+timerange+']'})
    # results = response.json()['data']['result']
    #
    # labelnames = set()
    # for result in results:
    #     labelnames.update(result['metric'].keys())
    #
    # labelnames.discard('__name__')
    # labelnames = sorted(labelnames)
    #
    # if writeHeader:
    #     writer.writerow(['name', 'timestamp', 'value'] + labelnames)
    #     writeHeader=False
    # for result in results:
    #     l = [result['metric'].get('__name__', '')] + result['values']
    #     for label in labelnames:
    #         l.append(result['metric'].get(label, ''))
    #         writer.writerow(l)
